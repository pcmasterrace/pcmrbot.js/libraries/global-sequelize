# Global Sequelize

This package wraps around the Sequelize constructor and stores the instance in the global namespace (referenced using ES6 Symbols), allowing all services using that database to share the same connection or pool of connections. 