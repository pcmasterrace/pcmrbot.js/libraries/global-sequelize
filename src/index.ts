import { Sequelize, Model } from "sequelize";
import * as crypto from "crypto";

/**
 * This class creates, stores, retrieves, and configures a Sequelize instance so all in-process services can share the same database connection.
 * 
 * @export 
 * @class GlobalSequelize
 */
export default class GlobalSequelize {
	private SEQUELIZE_KEY: symbol;
	private sha512: crypto.Hash;
    
    /**
     * Creates an instance of GlobalSequelize.
     * @param {string} connectionString
     * @memberof GlobalSequelize
     */
    constructor(connectionString: string, logger: any = false) {
        [this.SEQUELIZE_KEY, this.sha512] = this.constructSymbol(connectionString);

        let sequelize: Sequelize;
        if (Object.getOwnPropertySymbols(global).indexOf(this.SEQUELIZE_KEY) === -1) {
            sequelize = new Sequelize(connectionString, {logging: logger});

            global[this.SEQUELIZE_KEY] = {
				db: sequelize,
				models: {}
			};
        }
    }

    /**
     * Returns the Sequelize object for the associated database connnection
     * @memberof GlobalSequelize
     */
	get db(): Sequelize {
		return global[this.SEQUELIZE_KEY].db;
	}
	
	/**
	 * Returns the models object for the associated database connection
	 * @memberof GlobalSequelize
	 */
	get models(): {[key: string]: typeof Model} {
		return global[this.SEQUELIZE_KEY].models;
	}

	/**
	 * Computes and returns a SHA512 hash for the given connection string
	 * @memberof GlobalSequelize
	 */
	get hash(): string {
		return this.sha512.digest("hex");
	}

    /**
	 * Generates a symbol name for the specified database connection as to avoid making unnecessary connections
	 *
	 * @private
	 * @param {string} connectionString
	 * @returns {symbol}
	 * @memberof GlobalSequelize
	 */
	private constructSymbol(connectionString: string): [symbol, crypto.Hash] {
		let name = `pcmrbotjs.sequelize::${connectionString}`;
			
		const symbol = Symbol.for(name);
		const hash = crypto.createHash("sha512");
		hash.update(symbol.toString())

		return [symbol, hash]
	}
}