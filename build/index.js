"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const sequelize_1 = require("sequelize");
const crypto = tslib_1.__importStar(require("crypto"));
/**
 * This class creates, stores, retrieves, and configures a Sequelize instance so all in-process services can share the same database connection.
 *
 * @export
 * @class GlobalSequelize
 */
class GlobalSequelize {
    /**
     * Creates an instance of GlobalSequelize.
     * @param {string} connectionString
     * @memberof GlobalSequelize
     */
    constructor(connectionString, logger = false) {
        [this.SEQUELIZE_KEY, this.sha512] = this.constructSymbol(connectionString);
        let sequelize;
        if (Object.getOwnPropertySymbols(global).indexOf(this.SEQUELIZE_KEY) === -1) {
            sequelize = new sequelize_1.Sequelize(connectionString, { logging: logger });
            global[this.SEQUELIZE_KEY] = {
                db: sequelize,
                models: {}
            };
        }
    }
    /**
     * Returns the Sequelize object for the associated database connnection
     * @memberof GlobalSequelize
     */
    get db() {
        return global[this.SEQUELIZE_KEY].db;
    }
    /**
     * Returns the models object for the associated database connection
     * @memberof GlobalSequelize
     */
    get models() {
        return global[this.SEQUELIZE_KEY].models;
    }
    /**
     * Computes and returns a SHA512 hash for the given connection string
     * @memberof GlobalSequelize
     */
    get hash() {
        return this.sha512.digest("hex");
    }
    /**
     * Generates a symbol name for the specified database connection as to avoid making unnecessary connections
     *
     * @private
     * @param {string} connectionString
     * @returns {symbol}
     * @memberof GlobalSequelize
     */
    constructSymbol(connectionString) {
        let name = `pcmrbotjs.sequelize::${connectionString}`;
        const symbol = Symbol.for(name);
        const hash = crypto.createHash("sha512");
        hash.update(symbol.toString());
        return [symbol, hash];
    }
}
exports.default = GlobalSequelize;
