import { Sequelize, Model } from "sequelize";
/**
 * This class creates, stores, retrieves, and configures a Sequelize instance so all in-process services can share the same database connection.
 *
 * @export
 * @class GlobalSequelize
 */
export default class GlobalSequelize {
    private SEQUELIZE_KEY;
    private sha512;
    /**
     * Creates an instance of GlobalSequelize.
     * @param {string} connectionString
     * @memberof GlobalSequelize
     */
    constructor(connectionString: string, logger?: any);
    /**
     * Returns the Sequelize object for the associated database connnection
     * @memberof GlobalSequelize
     */
    get db(): Sequelize;
    /**
     * Returns the models object for the associated database connection
     * @memberof GlobalSequelize
     */
    get models(): {
        [key: string]: typeof Model;
    };
    /**
     * Computes and returns a SHA512 hash for the given connection string
     * @memberof GlobalSequelize
     */
    get hash(): string;
    /**
     * Generates a symbol name for the specified database connection as to avoid making unnecessary connections
     *
     * @private
     * @param {string} connectionString
     * @returns {symbol}
     * @memberof GlobalSequelize
     */
    private constructSymbol;
}
